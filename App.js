import React, {Component} from 'react';
import {
  Alert,
  AppState,
  FlatList, 
  NativeEventEmitter,
  NativeModules,
  Platform, 
  PermissionsAndroid, 
  ScrollView,
  StyleSheet, 
  Text,
  TouchableHighlight, 
  View
} from 'react-native';
import BleManager from 'react-native-ble-manager';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      scanning: false,
      peripherals: new Map(),
      appState: ''
    };

    this.handleAppStateChange = this.handleAppStateChange.bind(this);
    this.handleDiscoverPeripheral = this.handleDiscoverPeripheral.bind(this);
    this.handleStopScan = this.handleStopScan.bind(this);    
    this.handleDisconnectedPeripheral = this.handleDisconnectedPeripheral.bind(this);
    this.handleUpdateValueForCharacteristic = this.handleUpdateValueForCharacteristic.bind(this);    
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);

    BleManager.start({showAlert: false, forceLegacy: true})
    .then(() => {
      console.log('Module initialized');
    }, () => {
      console.log('Module failed');
    });

    this.handlerDiscover = bleManagerEmitter.addListener('BleManagerDiscoverPeripheral', this.handleDiscoverPeripheral);
    this.handlerStop = bleManagerEmitter.addListener('BleManagerStopScan', this.handleStopScan);
    this.handlerDisconnect = bleManagerEmitter.addListener('BleManagerDisconnectPeripheral', this.handleDisconnectedPeripheral);
    this.handlerUpdate = bleManagerEmitter.addListener('BleManagerDidUpdateValueForCharacteristic', this.handleUpdateValueForCharacteristic);

    if (Platform.OS === 'android' && Platform.Version >= 23) {
      PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
          if (result) {
            console.log("Permission is OK");
          } else {
            PermissionsAndroid.requestPermission(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION).then((result) => {
              if (result) {
                console.log("User accept");
              } else {
                console.log("User refuse");
              }
            });
          }
      });
    }    
  }

  componentWillUnmount() {
    this.handlerDiscover.remove();
    this.handlerStop.remove();
    this.handlerDisconnect.remove();
    this.handlerUpdate.remove();
  }

  render() {
    const list = Array.from(this.state.peripherals.values());

    return (
      <View style={{flex: 1}}>     
        <View>
          <Text>Welcome to Moska's Devices Manager</Text>
          <TouchableHighlight onPress={() => this.startScan()}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>Scan Bluetooth ({this.state.scanning ? 'on' : 'off'})</Text>
            </View>
          </TouchableHighlight>
          <TouchableHighlight onPress={() => this.retrieveConnected()}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>Retrieve connected peripherals</Text>
            </View>            
          </TouchableHighlight>
          <Text>Available Devices</Text>
          <ScrollView>
            {(list.length == 0) &&
                <View>
                  <Text>No peripherals</Text>
                </View>
            }
            <FlatList
              data={list}
              renderItem={({item}) => this.renderListItem(item)}
              keyExtractor={(item, index) => index.toString()}
            />
          </ScrollView>        
        </View>
      </View>
    );
  }

  renderListItem(item) {
    const color = item.connected ? 'green' : '#fff';
    const itemName = item.name ? item.name : '[ Device unknown ]';

    return (
      <TouchableHighlight onPress={() => this.test(item)}>
        <View style={[styles.row, {backgroundColor: color}]}>
          <Text>{itemName}</Text>
          <Text>{item.id}</Text>
        </View>
      </TouchableHighlight>
    );
  }

  test(peripheral) {
    if (peripheral){
      if (peripheral.connected){
        BleManager.disconnect(peripheral.id);
      }else{
        BleManager.connect(peripheral.id)
        .then(() => {
          let peripherals = this.state.peripherals;
          let p = peripherals.get(peripheral.id);
          
          if (p) {
            p.connected = true;
            peripherals.set(peripheral.id, p);
            this.setState({peripherals});
          }
          
          console.log('Connected to ' + peripheral.id);
          Alert.alert('Connected to ' + peripheral.id);
          
          setTimeout(() => {
            // Test read current RSSI value
            BleManager.retrieveServices(peripheral.id).then((peripheralData) => {
              console.log('Retrieved peripheral services', peripheralData);
              console.warn(peripheralData);
              BleManager.readRSSI(peripheral.id).then((rssi) => {
                console.log('Retrieved actual RSSI value', rssi);
                console.warn(rssi);
              });
            });

            // Test using bleno's pizza example
            // https://github.com/sandeepmistry/bleno/tree/master/examples/pizza
            /*BleManager.retrieveServices(peripheral.id).then((peripheralInfo) => {
              console.log(peripheralInfo);
              var service = '13333333-3333-3333-3333-333333333337';
              var bakeCharacteristic = '13333333-3333-3333-3333-333333330003';
              var crustCharacteristic = '13333333-3333-3333-3333-333333330001';

              setTimeout(() => {
                BleManager.startNotification(peripheral.id, service, bakeCharacteristic).then(() => {
                  console.log('Started notification on ' + peripheral.id);
                  Alert.alert('Started notification on ' + peripheral.id);
                  
                  setTimeout(() => {
                    BleManager.write(peripheral.id, service, crustCharacteristic, [0]).then(() => {
                      console.log('Writed NORMAL crust');
                      Alert.alert('Writed NORMAL crust');
                      BleManager.write(peripheral.id, service, bakeCharacteristic, [1,95]).then(() => {
                        console.log('Writed 351 temperature, the pizza should be BAKED');
                        Alert.alert('Writed 351 temperature, the pizza should be BAKED');
                      });
                    });
                  }, 500);

                }).catch((error) => {
                  console.log('Notification error', error);
                  Alert.alert('Notification error:' + error);
                });
              }, 200);

            });*/

          }, 900);
        }).catch((error) => {
          console.log('Connection error', error);
          Alert.alert('Connection error:' + error);
        });
      }
    }
  }

  handleAppStateChange(nextAppState) {
    if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
      console.log('App has come to the foreground!')
      BleManager.getConnectedPeripherals([]).then((peripheralsArray) => {
        console.log('Connected peripherals: ' + peripheralsArray.length);
        Alert.alert('Connected peripherals: ' + peripheralsArray.length);
      });
    }
    
    this.setState({appState: nextAppState});
  }

  handleDiscoverPeripheral(peripheral){
    var peripherals = this.state.peripherals;
    if (!peripherals.has(peripheral.id)){
      console.log('Got ble peripheral', peripheral);
      peripherals.set(peripheral.id, peripheral);
      this.setState({ peripherals });
    }
  }
  
  handleStopScan() {
    console.log('Scan is stopped');
    this.setState({ scanning: false });
  }

  handleDisconnectedPeripheral(data) {
    let peripherals = this.state.peripherals;
    let peripheral = peripherals.get(data.peripheral);
    if (peripheral) {
      peripheral.connected = false;
      peripherals.set(peripheral.id, peripheral);
      this.setState({peripherals});
    }
    console.log('Disconnected from ' + data.peripheral);
    Alert.alert('Disconnected from ' + data.peripheral);
  }

  handleUpdateValueForCharacteristic(data) {
    console.log('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
    Alert.alert('Received data from ' + data.peripheral + ' characteristic ' + data.characteristic, data.value);
  }  

  startScan() {
    if (!this.state.scanning) {
      this.setState({peripherals: new Map()});
      BleManager.scan([], 10, true).then((results) => {
        console.log('Scanning...');
        this.setState({scanning:true});
      });
    }
  }

  retrieveConnected(){
    BleManager.getConnectedPeripherals([]).then((results) => {
      if (results.length == 0) {
        console.log('No connected peripherals');
        Alert.alert('No connected peripherals');
      }
      console.log(results);
      var peripherals = this.state.peripherals;
      for (var i = 0; i < results.length; i++) {
        var peripheral = results[i];
        peripheral.connected = true;
        peripherals.set(peripheral.id, peripheral);
        this.setState({ peripherals });
      }
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,    
    backgroundColor: '#FFFFFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  button: {
    marginBottom: 30,
    width: 260,
    alignItems: 'center',
    backgroundColor: '#2196F3'
  },
  buttonText: {
    padding: 20,
    color: 'white'
  },
});
